const thousandify = require('../src/index')

describe('Current thousand format', () => {
    test('1000 -> 1,000', () => {
        expect(thousandify(1000)).toBe('1,000')
    })

    test('1000 -> 1,000.00', () => {
        expect(thousandify(1000, { decimalDigits: 2 })).toBe('1,000.00')
    })

    test('1000.0 -> 1,000.10', () => {
        expect(thousandify(1000.1, { decimalDigits: 2 })).toBe('1,000.10')
    })

    test('123456 -> 123,456', () => {
        expect(thousandify(123456)).toBe('123,456')
    })

    test('123456.123 -> 123,456.123', () => {
        expect(thousandify(123456.123)).toBe('123,456.123')
    })

    test('123456.123 -> 123,456.1', () => {
        expect(thousandify(123456.123, { decimalDigits: 1 })).toBe('123,456.1')
    })

    test('123456.123 -> 123,456', () => {
        expect(thousandify(123456.123, { decimalDigits: 0 })).toBe('123,456')
    })

    test('123456.123 -> 123 456', () => {
        expect(thousandify(123456.123, { decimalDigits: 0, thousandSeparator: ' ' })).toBe('123 456')
    })
})

describe('Digit string', () => {
    test('1000 should return 1,000', () => {
        expect(thousandify('1000')).toBe('1,000')
    })

    test('1000 should return 1,000.00', () => {
        expect(thousandify('1000', { decimalDigits: 2 })).toBe('1,000.00')
    })
})

describe('Invalid param', () => {
    test('NaN should return NaN', () => {
        expect(thousandify(NaN)).toBe(NaN)
    })

    test('null should return null', () => {
        expect(thousandify(null)).toBe(null)
    })

    test('undefined should return undefined', () => {
        expect(thousandify(undefined)).toBe(undefined)
    })
})