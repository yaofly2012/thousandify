
const toString = Object.prototype.toString;

const charsetRegExp = /^[\d\.]+$/;

const defaultOption = {
    thousandSeparator: ',', // 千分位分隔符
    decimalSeparator: '.', // 小数分隔符
    decimalDigits: false // 小数位数量, false表示不特殊处理
}

module.exports = exports = thousandify;

function isNumber(target) {
    return toString.call(target) === '[object Number]'
}

function thousandify(amount, option) {    
    const parsedAmount = amount + '';

    if(!charsetRegExp.test(parsedAmount)) {
        return amount
    }

    const { 
        decimalSeparator, 
        thousandSeparator,
        decimalDigits
    } = Object.assign({}, defaultOption, option);

    const amountParts = parsedAmount.split(decimalSeparator);
    const intPartStr = amountParts[0].replace(/(?!^)(?=(\d{3})+$)/g, thousandSeparator);
    
    // 处理小数部分
    let decimalStr = amountParts[1] || '';    
    if(isNumber(decimalDigits)) {
        decimalStr = (decimalStr + Array(decimalDigits + 1).join(0)).substr(0, decimalDigits);
    }

    return !!decimalStr
        ? [intPartStr, decimalStr].join(decimalSeparator) 
        : intPartStr;
}